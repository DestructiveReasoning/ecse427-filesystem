#ifndef _INCLUDE_SFS_API_H_
#define _INCLUDE_SFS_API_H_

#include <stdint.h>

#define MAXFILENAME 20
#define MAX_EXTENSION_NAME 3

#define BLOCK_SIZE 1024
#define MAX_INODES 100

#define LOC_SUPERBLOCK 0
#define LOC_INODE_TABLE 1
#define LOC_DATA_BLOCKS 11
#define LOC_BITMAP 1023

typedef struct superblock_t{
    uint64_t magic;
    uint64_t block_size;
    uint64_t fs_size;
    uint64_t inode_table_len;
    uint64_t root_dir_inode;
} superblock_t;

typedef struct inode_t {
    unsigned int mode;
    unsigned int link_cnt;
    unsigned int uid;
    unsigned int gid;
    unsigned int size;
    unsigned int data_ptrs[12];
    unsigned int indirectPointer; // points to a data block that points to other data blocks (Single indirect)
} inode_t;

/*
 * inodeIndex    which inode this entry describes
 * inode  pointer towards the inode in the inode table
 *rwptr    where in the file to start   
 */
typedef struct file_descriptor {
    uint64_t inodeIndex;
    inode_t* inode;
    uint64_t rwptr;
} file_descriptor;


typedef struct directory_entry{
    int num; // represents the inode number of the entery. 
    char name[MAXFILENAME]; // represents the name of the entery. 
}directory_entry;

typedef struct fd_table {
	file_descriptor fds[MAX_INODES - 1];
	uint8_t open[13]; //Bitmap of available file descriptor ids
} fd_table;

typedef struct inode_table_t {
	inode_t inodes[MAX_INODES];
	uint8_t open[13]; //Bitmap of available inode numbers
} inode_table_t;

inode_table_t inode_table;
directory_entry directory[MAX_INODES - 1];
fd_table file_descriptor_table;
unsigned int file_pointer;

int get_next_open(uint8_t [], int max); //Gets location of first 1 bit in bitmap of width `max`
void create_superblock(superblock_t *);
void create_inode(inode_t *);
void create_fd(file_descriptor *fd, uint64_t inodeIndex);
void insert_fd(file_descriptor fd, int index);
void init_inode_table(void);
void init_fdtable(void);

int valid_file_name(char *);

void mksfs(int fresh);
int sfs_getnextfilename(char *fname);
int sfs_getfilesize(const char* path);
int sfs_fopen(char *name);
int sfs_fclose(int fileID);
int sfs_fread(int fileID, char *buf, int length);
int sfs_fwrite(int fileID, const char *buf, int length);
int sfs_fseek(int fileID, int loc);
int sfs_remove(char *file);

#endif //_INCLUDE_SFS_API_H_
