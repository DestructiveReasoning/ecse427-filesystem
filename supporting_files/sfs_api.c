#include "sfs_api.h"
#include "bitmap.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fuse.h>
#include <strings.h>
#include "disk_emu.h"
#define LASTNAME_FIRSTNAME_DISK "WILTZER_HARLEY_DISK.disk"
#define NUM_BLOCKS 1024  //maximum number of data blocks on the disk.
#define BITMAP_ROW_SIZE (NUM_BLOCKS/8) // this essentially mimcs the number of rows we have in the bitmap. we will have 128 rows. 

/* macros */
#define FREE_BIT(_data, _which_bit) \
    _data = _data | (1 << _which_bit)

#define USE_BIT(_data, _which_bit) \
    _data = _data & ~(1 << _which_bit)

/*
 * Returns 1 when proposed file name is valid, and 0 otherwise
 * Valid file names have at most MAX_EXTENSION_NAME characters for the extension
 * Valid file names are at most MAXFILENAME bytes long
 */
int valid_file_name(char *fname) {
	int c;
	int size = strlen(fname);
	if(size > MAXFILENAME) return 0; //File name is too long
	for(c = 0; fname[c] != '.' && fname[c] != '\0'; c++); //Counts characters before extension
	if(size - c - 1 > MAX_EXTENSION_NAME) return 0; //Extension is too long
	return 1;
}

/*
 * Returns the index of the first HIGH bit (value of 1) in a bitmap of width `max`
 * Returns -1 if all bits in the bitmap are LOW
 */
int get_next_open(uint8_t open[], int max) {
	for(int c = 0; c < max; c++) {
		//Get index by dividing byte number by 8 (same as c >> 3)
		//Put wanted byte in LSB by shifting right by c % 8 (same as c & 7)
		//Check if LSB is 1, indicating the wanted byte is HIGH
		if((open[c >> 3] >> (c & 7)) & 1) {
			return c;
		}
	}
	return -1;
}

/*
 * Populates a superblock_t with default values
 */
void create_superblock(superblock_t *block) {
	block->magic = 0xACBD0005;
	block->block_size = BLOCK_SIZE;
	block->fs_size = BLOCK_SIZE * BLOCK_SIZE; //There happens to be the same amount of blocks as bytes/block
	block->inode_table_len = 10; //Allocate 10 blocks for the inode table, to ensure no overflow
	block->root_dir_inode = 0; //Root dir is in first data block after inode table
}

/*
 * Populates a new inode with default values
 */
void create_inode(inode_t *inode) {
	inode->mode = 0; //Not used in this assignment
	inode->link_cnt = 1; //Link count is 1 for files with no extra links, as per ls manpage
	inode->uid = 0; //Not used in this assignment
	inode->gid = 0; //Not used in this assignment
	inode->size = 0; //Initially, all files are created empty
	for(int c = 0; c < 12; c++) {
		inode->data_ptrs[c] = 0;
	}
	inode->indirectPointer = 0;
}

/*
 * Populates a new file descriptor and does requisite maintenance on the associated inode
 */
void create_fd(file_descriptor *fd, uint64_t inodeIndex) {
	fd->inodeIndex = inodeIndex;
	fd->inode = &inode_table.inodes[inodeIndex];
	fd->rwptr = fd->inode->size;//Files are opened in append mode, so rwptr should be at the end of the file
}

/*
 * Inserts a file descriptor into the file descriptor table at index `index`
 */
void insert_fd(file_descriptor fd, int index) {
	file_descriptor_table.fds[index] = fd;
	file_descriptor_table.open[index >> 3] &= ~(1 << (index & 7)); //Mark the corresponding fd bit as LOW
}

/*
 * Initializes a fresh inode table
 */
void init_inode_table() {
	memset(&inode_table.inodes, 0, MAX_INODES * sizeof(inode_t)); //Zero everything out so no 'garbage' is included in inode areas
	inode_table.open[0] = 0xFE; //Mark bit 0 as LOW, because the first inode points to the directory
	for(int c = 1; c < 13; c++) {
		inode_table.open[c] = 0xFF; //Mark all other bits as HIGH, because all other inode numbers are free for a fresh table
	}
}

/*
 * Initializes a fresh file descriptor table
 */
void init_fdtable() {
	for(int c = 0; c < 13; c++) {
		file_descriptor_table.open[c] = 0xff; //Mark all bits as HIGH, because there are no fds in use
	}
	//Zero out the file descriptor table to remove any 'garbage'
	memset(&file_descriptor_table.fds, 0, (MAX_INODES - 1) * sizeof(file_descriptor));
}

//initialize all bits to high
uint8_t free_bit_map[BITMAP_ROW_SIZE] = { [0 ... BITMAP_ROW_SIZE - 1] = UINT8_MAX };

void mksfs(int fresh) {
	if(!fresh) {
		init_disk(LASTNAME_FIRSTNAME_DISK, BLOCK_SIZE, BLOCK_SIZE);

		superblock_t superblock;
		//Read superblock data into buffer first, so it can be copied into a superblock_t bytewise
		char superblock_buf[BLOCK_SIZE]; 
		read_blocks(LOC_SUPERBLOCK, 1, superblock_buf);
		memcpy(&superblock, superblock_buf, sizeof(superblock_t));

		//Read inode table data into buffer first, so it can be copied into inode_table bytewise
		void *inode_table_buf = malloc(superblock.inode_table_len * BLOCK_SIZE);
		read_blocks(LOC_INODE_TABLE, (int) superblock.inode_table_len, inode_table_buf);
		memcpy(&inode_table, inode_table_buf, sizeof(inode_table));
		free(inode_table_buf); //Free heap data, no longer needed

		//Read directory table data into buffer first, so it can be copied into directory bytewise
		char directory_buf[3 * BLOCK_SIZE];
		read_blocks(1 + superblock.inode_table_len + inode_table.inodes[superblock.root_dir_inode].data_ptrs[0], 3, directory_buf);
		memcpy(&directory, directory_buf, sizeof(directory));

		//The bitmap data can be copied straight into free_bit_map, because its size is a perfect multiple of BLOCK_SIZE
		read_blocks(LOC_BITMAP, 1, free_bit_map);
	} 
	else {
		init_fresh_disk(LASTNAME_FIRSTNAME_DISK, BLOCK_SIZE, BLOCK_SIZE);

		superblock_t superblock;
		create_superblock(&superblock);
		write_blocks(LOC_SUPERBLOCK, 1, &superblock);

		for(int c = 0; c < MAX_INODES; c++) {
			create_inode(&inode_table.inodes[c]);
		}
		for(int c = 0; c < MAX_INODES - 1; c++) {
			directory[c].num = c + 1;
			memset(directory[c].name, 0, MAXFILENAME);
		}
		init_inode_table();
		inode_table.inodes[0].size = (sizeof(directory_entry) * MAX_INODES);
		//Set root dir inode's data ptrs to point to the blocks storing the directory
		//Directory is 3 block wide and starts at the first data block
		inode_table.inodes[0].data_ptrs[0] = 0;
		inode_table.inodes[0].data_ptrs[1] = 1;
		inode_table.inodes[0].data_ptrs[2] = 2;
		free_bit_map[0] = 0xF8; //0xF8 = 0b11111000 -> Mark first 3 bits as LOW because they are occupied by the directory

		write_blocks(LOC_INODE_TABLE, 10, &inode_table);
		write_blocks(LOC_DATA_BLOCKS, 3, &directory);
		write_blocks(LOC_BITMAP, 1, &free_bit_map);
	}
	init_fdtable(); //Whether or not the disk is fresh, file descriptor table always starts fresh - not stored on disk
	file_pointer = 0; //Points to the current file (for getnextfilename), start at first file in directory
}
int sfs_getnextfilename(char *fname){
	//Loop over empty directory entries until a file is found
	while(file_pointer < MAX_INODES - 1){
		if(directory[file_pointer].name == NULL || directory[file_pointer].name[0] == 0) {
			file_pointer++;
			continue;
		}
		memcpy(fname, directory[file_pointer++].name, MAXFILENAME);
		fname[MAXFILENAME] = '\0';
		return 1;
	}
	//Reset to beginning and return 0 when no files remain
	file_pointer = 0;
	return 0;
}
int sfs_getfilesize(const char* path){
	//Search for referenced file
	for(int c = 0; c < MAX_INODES - 1; c++) {
		if(directory[c].name != NULL && strncmp(directory[c].name, path, MAXFILENAME) == 0) {
			int inum = directory[c].num;
			return inode_table.inodes[inum].size;
		}
	}
	//If file isn't found in the directory, return -1
	return -1;
}
int sfs_fopen(char *name){
	if(!valid_file_name(name)) {
		return -1; //Avoid opening illegal file names
	}
	for(int c = 0; c < MAX_INODES - 1; c++) {
		//Search for existing file
		if(directory[c].name != NULL && strncmp(directory[c].name, name, MAXFILENAME) == 0) {
			//Found file in directory
			int inum = directory[c].num;
			for(int j = 0; j < MAX_INODES - 1; j++) {
				//Check if file is already open (in file descriptor table)
				if(&file_descriptor_table.fds[j] != NULL) {
					if(file_descriptor_table.fds[j].inodeIndex == inum) {
						file_descriptor_table.open[j >> 3] &= ~(1 << (j & 7));
						return j;
					}
				}
			}
			//If file wasn't in fd table, make an entry for it
			int newfd = get_next_open(file_descriptor_table.open, 100);
			file_descriptor f;
			if(newfd == -1) return -1;
			create_fd(&f, (uint64_t )inum);
			insert_fd(f, newfd);
			return newfd;
		}
	}
	int newfd = get_next_open(file_descriptor_table.open, 100);
	if(newfd == -1) {
		return -1; //Fail if all file descriptors are in use
	}
	int next_inode = get_next_open(inode_table.open, MAX_INODES);
	if(next_inode == -1) {
		return -1; //Fail if all inodes are in use
	}
	read_blocks(LOC_BITMAP, 1, free_bit_map); //Get latest bitmap
	int next_block = get_next_open(free_bit_map, 1024);
	if(next_block == -1) {
		return -1; //Fail if all blocks are in use
	}
	//Load superblock to get disk format
	superblock_t superblock;
	char buf[BLOCK_SIZE];
	read_blocks(0,1,buf);
	memcpy(&superblock,buf,sizeof(superblock));

	inode_table.inodes[next_inode].data_ptrs[0] = next_block; //Give new file access to a block for data
	inode_table.open[next_inode >> 3] &= ~(1 << (next_inode & 7)); //Mark new inode as taken
	write_blocks(LOC_INODE_TABLE, 10, &inode_table); //Update the inode table on disk
	//Update the directory in memory
	memcpy(directory[next_inode - 1].name, name, MAXFILENAME); //Index next_inode-1 so that directory table doesn't need an entry for the inode pointing to the directory table
	//Update the directory on disk
	write_blocks(1 + superblock.inode_table_len + inode_table.inodes[superblock.root_dir_inode].data_ptrs[0], 3, &directory);
	//Create new fd for the new file and add it to the table
	file_descriptor f;
	create_fd(&f, (uint64_t)next_inode);
	insert_fd(f, newfd);
	free_bit_map[next_block >> 3] &= ~(1 << (next_block & 7)); //Mark allocated data block as taken
	write_blocks(LOC_BITMAP, 1, free_bit_map); //Update the free bit map on disk
	return newfd;
}

int sfs_fwrite(int fileID, const char *buf, int length) {
	if((file_descriptor_table.open[fileID >> 3] >> (fileID & 7)) & 1) {
		//Fail if the file descriptor isn't open
		return -1;
	}
	file_descriptor *file = &file_descriptor_table.fds[fileID];
	if(file->rwptr + length > BLOCK_SIZE * (12 + BLOCK_SIZE/sizeof(unsigned int))) {
		//Fail if the length of the file after the write is too large
		//Each inode has 12 data pointers and a maximum of BLOCK_SIZE/sizeof(unsigned int) indirect pointers
		//Therefore, a file cannot require more than 12 + 256 blocks
		return -1;
	}

	//Load the superblock to get format data
	superblock_t superblock;
	char buffer[BLOCK_SIZE];
	read_blocks(LOC_SUPERBLOCK, 1, buffer);
	memcpy(&superblock, buffer, sizeof(superblock_t));

	int blocks_owned = 0; //Stores the amount of data blocks currently held by the inode
	for(int c = 0; c < 12; c++) {
		if(file->inode->data_ptrs[c] != 0) blocks_owned++;
		else break;
	}
	if(file->inode->indirectPointer != 0) {
		blocks_owned++; //To account for the indirect pointer block itself
		unsigned int indirectBlocks[BLOCK_SIZE/sizeof(unsigned int)];
		read_blocks(1 + superblock.inode_table_len + file->inode->indirectPointer, 1, indirectBlocks);
		for(int c = 0; c < BLOCK_SIZE/sizeof(unsigned int); c++) {
			if(indirectBlocks[c] != 0) blocks_owned++;
			else break;
		}
	}

	int final_rwptr = file->rwptr + length;
	int blocks_needed = (final_rwptr % BLOCK_SIZE == 0) ? final_rwptr / BLOCK_SIZE : final_rwptr / BLOCK_SIZE + 1; //Effective ceiling operation
	if(blocks_needed > 12) blocks_needed++; //Accounts for the needed indirect pointer block
	int blocks_to_acquire = (blocks_needed > blocks_owned) ? blocks_needed - blocks_owned : 0; //Amount of extra blocks necessary to perform write
	int *free_blocks = (int *)malloc(blocks_to_acquire * sizeof(int)); //Will store addresses of free blocks for use
	int free_block_count = 0; //Counts how many free blocks were found
	for(int c = 0; c < 1024 && free_block_count < blocks_to_acquire; c++) {
		if((free_bit_map[c >> 3] >> (c & 7)) & 1) { //Check if cth bit is HIGH
			free_blocks[free_block_count++] = c;
		}
	}
	if(free_block_count < blocks_to_acquire) {
		//Fail if there are not enough free blocks on disk
		free(free_blocks);
		return -1;
	}
	int data_block_offset = 1 + superblock.inode_table_len;
	int start_block = file->rwptr / BLOCK_SIZE; //Finds which block (relative to inode) that the rwptr is in
	int blocks_taken = 0; //Counts how many blocks have been taken during the write
	int bytes_written = 0; //Counts how many bytes have been written during the write
	unsigned int indirectBlocks[BLOCK_SIZE/sizeof(unsigned int)]; //Stores the indirect block pointers
	int allocatedIndirect = 0; //Determines if the indirect blocks array has been populated yet
	int cur_block;
	for(int block = start_block; block < blocks_needed && file->rwptr < final_rwptr; block++) { //Cycle through all blocks that will be modified
		int byte_offset = file->rwptr % BLOCK_SIZE; //Finds the byte index within a block that the rwptr points to
		int bytes_to_write = (BLOCK_SIZE - byte_offset + bytes_written > length) ? length - bytes_written : BLOCK_SIZE - byte_offset;
		if(block < 12) {
			cur_block = (block < blocks_owned) ? file->inode->data_ptrs[block] : free_blocks[blocks_taken++];
			if(block < blocks_owned) { //If we already own the current block...
				read_blocks(data_block_offset + cur_block, 1, buffer);
			} else { //We took a new block
				free_bit_map[cur_block >> 3] &= ~(1 << (cur_block & 7)); //Mark new block as taken
				memset(buffer, 0, BLOCK_SIZE); //Zero out buffer to remove garbage
				file->inode->data_ptrs[block] = cur_block; //Store new block address
			}
		} else { //Now we are dealing with indirect pointers
			if(!allocatedIndirect) { //Load up (or create) indirect pointers if not already done
				if(file->inode->indirectPointer == 0) { //Indirect pointer is empty
					file->inode->indirectPointer = free_blocks[blocks_taken++]; //Get new block
					free_bit_map[file->inode->indirectPointer >> 3] &= ~(1 << (file->inode->indirectPointer & 7)); //Mark new block as taken
					memset(indirectBlocks,0,BLOCK_SIZE); //Zero out new block to remove garbage
				} else {
					read_blocks(data_block_offset + file->inode->indirectPointer, 1, indirectBlocks);
				}
				allocatedIndirect = 1;
			}
			if(indirectBlocks[block - 12] == 0) { //If the current indirect pointer doesn't point to a data block...
				indirectBlocks[block-12] = free_blocks[blocks_taken++]; //...get new block
				free_bit_map[indirectBlocks[block-12] >> 3] &= ~(1 << (indirectBlocks[block-12] & 7)); //Mark new block as taken
				memset(buffer, 0, BLOCK_SIZE);
			} else {
				read_blocks(data_block_offset + indirectBlocks[block-12], 1, buffer);
			}
			cur_block = indirectBlocks[block-12];
			write_blocks(data_block_offset + file->inode->indirectPointer, 1, indirectBlocks);
		}
		memcpy(buffer + byte_offset, buf + bytes_written, bytes_to_write);
		write_blocks(data_block_offset + cur_block, 1, buffer);
		file->rwptr += bytes_to_write; //Keep rwptr at the end of written data
		bytes_written += bytes_to_write; //Update bytes written
	}
	free(free_blocks); //Free blocks have now been allocated, so the heap array is no longer needed
	if(final_rwptr > file->inode->size) file->inode->size = final_rwptr; //If rwptr is beyond the file's previous size, update size to rwptr. Otherwise file hasn't changed size
	write_blocks(LOC_BITMAP, 1, free_bit_map); //Update the free bit map on disk
	if(file->inode->indirectPointer != 0) write_blocks(data_block_offset + file->inode->indirectPointer, 1, indirectBlocks); //If the inode has indirect blocks, write the indirect pointer to disk
	write_blocks(1, superblock.inode_table_len, &inode_table); //Update the inode table on disk
	return length;
}

int sfs_fclose(int fileID) {
	if((file_descriptor_table.open[fileID >> 3] >> (fileID & 7)) & 1) return -1; //Fail if file isn't already open
	file_descriptor_table.open[fileID >> 3] |= (1 << (fileID & 7)); //Mark file descriptor id as free
	file_descriptor_table.fds[fileID].rwptr = 0; //Reset the rwptr to 0
	return 0;
}
int sfs_fread(int fileID, char *buf, int length) {
	if((file_descriptor_table.open[fileID >> 3] >> (fileID & 7)) & 1) {
		//Fail if file isn't open
		return -1;
	}
	//Read superblock for disk format data
	superblock_t superblock;
	char buffer[BLOCK_SIZE];
	read_blocks(0, 1, buffer);
	memcpy(&superblock, buffer, sizeof(superblock));

	inode_t *f_inode = file_descriptor_table.fds[fileID].inode;
	if(f_inode->size < length) length = f_inode->size;
	length += file_descriptor_table.fds[fileID].rwptr;
	file_descriptor *file = &file_descriptor_table.fds[fileID];
	int startrwptr = file->rwptr;
	//Read data from data_ptrs
	while(file->rwptr < 12 * BLOCK_SIZE && file->rwptr < length) {
		uint64_t space_in_block = BLOCK_SIZE - (file->rwptr % BLOCK_SIZE);
		int remaining = (length - file->rwptr < space_in_block) ? length - file->rwptr : space_in_block;
		char next_block[BLOCK_SIZE];
		unsigned int next_block_addr = 1 + superblock.inode_table_len + f_inode->data_ptrs[file->rwptr/BLOCK_SIZE];
		read_blocks(next_block_addr, 1, next_block);
		memcpy(buf + file->rwptr - startrwptr, next_block + BLOCK_SIZE - space_in_block, remaining);
		file->rwptr += remaining;
	}
	if(file->rwptr < length) { //If there is still data to be read, look through indirect pointer
		int indirect_block_addr = 1 + superblock.inode_table_len + f_inode->indirectPointer;
		int extra_block_addrs[BLOCK_SIZE/sizeof(unsigned int)];
		read_blocks(indirect_block_addr, 1, extra_block_addrs);
		int startindex = (file->rwptr - 12 * BLOCK_SIZE) / BLOCK_SIZE;
		for(int index = startindex; index < BLOCK_SIZE/sizeof(unsigned int) && file->rwptr < length; index++) {
			uint64_t space_in_block = BLOCK_SIZE - (file->rwptr % BLOCK_SIZE);
			int remaining = (length - file->rwptr < space_in_block) ? length - file->rwptr : space_in_block;
			char next_block[BLOCK_SIZE];
			unsigned int next_block_addr = 1 + superblock.inode_table_len + extra_block_addrs[index];
			read_blocks(next_block_addr, 1, next_block);
			memcpy(buf + file->rwptr - startrwptr, next_block + BLOCK_SIZE - space_in_block, remaining);
			file->rwptr += remaining;
		}
	}
	return file->rwptr - startrwptr;
}
int sfs_fseek(int fileID, int loc) {
	if((file_descriptor_table.open[fileID >> 3] >> (fileID & 7)) & 1) {
		return -1; //Fail if file isn't open
	}
	file_descriptor_table.fds[fileID].rwptr = loc;
	return loc;
}
int sfs_remove(char *file) {
	int inum = -1;
	//Look for file in directory
	for(int c = 0; c < MAX_INODES - 1; c++) {
		if(directory[c].name != NULL && strncmp(directory[c].name, file, MAXFILENAME) == 0) {
			inum = directory[c].num; //Update inum to the file's associated inode number
			memset(directory[c].name, 0, MAXFILENAME); //Zero out file name in directory
			break;
		}
	}
	if(inum == -1) {
		return -1; //File not found, so fail
	}
	inode_t inode = inode_table.inodes[inum];

	//Read superblock for disk format
	superblock_t superblock;
	char buf[BLOCK_SIZE];
	read_blocks(0,1,buf);
	memcpy(&superblock, buf, sizeof(superblock));

	//Mark inode as open
	inode_table.open[inum >> 3] |= (1 << (inum & 7));
	unsigned int size = inode.size;
	unsigned int rounded_size = size + (BLOCK_SIZE - (size % BLOCK_SIZE)); //Get size in a multiple of BLOCK_SIZE
	if(size % BLOCK_SIZE == 0) rounded_size -= BLOCK_SIZE; //Corrects in case size is a multiple of BLOCK_SIZE. This and the above line do a ceiling operation
	//Mark inode's data blocks as open
	int c; //Keeps track of block to free
	for(c = 0; c < 12 && c < rounded_size / BLOCK_SIZE; c++) {
		unsigned int cur_block = inode.data_ptrs[c];
		free_bit_map[cur_block >> 3] |= (1 << (cur_block & 7)); //Mark current block as free
		inode_table.inodes[inum].data_ptrs[c] = 0; //Zero out address
	}
	inode_table.inodes[inum].indirectPointer = 0; //Zero out indirect pointer
	if(c < rounded_size / BLOCK_SIZE) {
		unsigned int extra_block_addrs[BLOCK_SIZE / sizeof(unsigned int)]; //Stores indirect data pointers
		read_blocks(1 + superblock.inode_table_len + inode.indirectPointer, 1, extra_block_addrs);
		for(; c < rounded_size / BLOCK_SIZE - 1; c++) {
			unsigned int cur_block = extra_block_addrs[c - 12];
			free_bit_map[cur_block >> 3] |= (1 << (cur_block & 7)); //Mark indirect block as free
		}
	}
	inode_table.inodes[inum].size = 0; //Reset size of inode to 0
	write_blocks(1, superblock.inode_table_len, &inode_table); //Update inode table on disk
	write_blocks(1 + superblock.inode_table_len + inode_table.inodes[superblock.root_dir_inode].data_ptrs[0], 3, &directory); //Update directory on disk
	write_blocks(superblock.fs_size / BLOCK_SIZE - 1, 1, free_bit_map); //Update free bit map on disk
	return 0;
}

